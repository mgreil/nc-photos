// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'archive_file.dart';

// **************************************************************************
// NpLogGenerator
// **************************************************************************

extension _$_SetArchiveFileNpLog on _SetArchiveFile {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("use_case.archive_file._SetArchiveFile");
}
