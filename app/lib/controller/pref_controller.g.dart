// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pref_controller.dart';

// **************************************************************************
// NpLogGenerator
// **************************************************************************

extension _$PrefControllerNpLog on PrefController {
  // ignore: unused_element
  Logger get _log => log;

  static final log = Logger("controller.pref_controller.PrefController");
}
